var searchData=
[
  ['opus_20custom_310',['Opus Custom',['../group__opus__custom.html',1,'']]],
  ['opus_20decoder_311',['Opus Decoder',['../group__opus__decoder.html',1,'']]],
  ['opus_20encoder_312',['Opus Encoder',['../group__opus__encoder.html',1,'']]],
  ['opus_20library_20information_20functions_313',['Opus library information functions',['../group__opus__libinfo.html',1,'']]],
  ['opus_20multistream_20api_314',['Opus Multistream API',['../group__opus__multistream.html',1,'']]]
];
