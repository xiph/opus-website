var searchData=
[
  ['ope_5fapi_5fversion_6',['OPE_API_VERSION',['../group__error__codes.html#gaabdb1ad63b0269baabad5e67e3bf209b',1,'opusenc.h']]],
  ['ope_5fclose_5ffunc_7',['ope_close_func',['../group__callbacks.html#gaa0fa1f368557f82a831909652855df5a',1,'opusenc.h']]],
  ['ope_5fcomments_5fadd_8',['ope_comments_add',['../group__comments.html#ga7abca6f2dddd45463089ff0ae056730c',1,'opusenc.h']]],
  ['ope_5fcomments_5fadd_5fpicture_9',['ope_comments_add_picture',['../group__comments.html#gaf0e224f7af264eb03043aac13e874113',1,'opusenc.h']]],
  ['ope_5fcomments_5fadd_5fpicture_5ffrom_5fmemory_10',['ope_comments_add_picture_from_memory',['../group__comments.html#gaf465cb4c582a81482eb11120f227097f',1,'opusenc.h']]],
  ['ope_5fcomments_5fadd_5fstring_11',['ope_comments_add_string',['../group__comments.html#gaf4c0912b9cbe4fd12a4797b66ff4e65d',1,'opusenc.h']]],
  ['ope_5fcomments_5fcopy_12',['ope_comments_copy',['../group__comments.html#ga94605d51b61e3d25140b83a25b2ca6c6',1,'opusenc.h']]],
  ['ope_5fcomments_5fcreate_13',['ope_comments_create',['../group__comments.html#ga3e41386cffb6f293a4d0f15c82545b43',1,'opusenc.h']]],
  ['ope_5fcomments_5fdestroy_14',['ope_comments_destroy',['../group__comments.html#gaab8b024d212f33738b9bf804009c9a1a',1,'opusenc.h']]],
  ['ope_5fencoder_5fchain_5fcurrent_15',['ope_encoder_chain_current',['../group__encoding.html#gaf35e74bf7bf70cf20408f4670286a483',1,'opusenc.h']]],
  ['ope_5fencoder_5fcontinue_5fnew_5fcallbacks_16',['ope_encoder_continue_new_callbacks',['../group__encoding.html#ga5a538fe59c802246596aba2f628eac53',1,'opusenc.h']]],
  ['ope_5fencoder_5fcontinue_5fnew_5ffile_17',['ope_encoder_continue_new_file',['../group__encoding.html#ga67ff890746eb72d9d9a04f9899fbe16d',1,'opusenc.h']]],
  ['ope_5fencoder_5fcreate_5fcallbacks_18',['ope_encoder_create_callbacks',['../group__encoding.html#ga88a41a7431161b70ab8da61971f38b41',1,'opusenc.h']]],
  ['ope_5fencoder_5fcreate_5ffile_19',['ope_encoder_create_file',['../group__encoding.html#ga2273916d02b5fd385885279806964852',1,'opusenc.h']]],
  ['ope_5fencoder_5fcreate_5fpull_20',['ope_encoder_create_pull',['../group__encoding.html#ga005548b01cc4f2c5082d0fa26a539b83',1,'opusenc.h']]],
  ['ope_5fencoder_5fctl_21',['ope_encoder_ctl',['../group__encoding.html#gae6401fad5b7c4fa999ec034fc78add9a',1,'opusenc.h']]],
  ['ope_5fencoder_5fdeferred_5finit_5fwith_5fmapping_22',['ope_encoder_deferred_init_with_mapping',['../group__encoding.html#gae0c3556843881b1f8e15991b96125e50',1,'opusenc.h']]],
  ['ope_5fencoder_5fdestroy_23',['ope_encoder_destroy',['../group__encoding.html#gab4bc97f642adf52f9ab4fcb030125146',1,'opusenc.h']]],
  ['ope_5fencoder_5fdrain_24',['ope_encoder_drain',['../group__encoding.html#ga3828690aeeb8d0d4f4c47a81240d7b61',1,'opusenc.h']]],
  ['ope_5fencoder_5fflush_5fheader_25',['ope_encoder_flush_header',['../group__encoding.html#ga9bb37b5f86fa2c8b192717ff7965c27a',1,'opusenc.h']]],
  ['ope_5fencoder_5fget_5fpage_26',['ope_encoder_get_page',['../group__encoding.html#ga43f8e3711659166c5259c44e245e9876',1,'opusenc.h']]],
  ['ope_5fencoder_5fwrite_27',['ope_encoder_write',['../group__encoding.html#gacad7b5d50d5f468847f93c7242fc606d',1,'opusenc.h']]],
  ['ope_5fencoder_5fwrite_5ffloat_28',['ope_encoder_write_float',['../group__encoding.html#ga4521ae821a7212ae52b0fb0475348ac0',1,'opusenc.h']]],
  ['ope_5fget_5fabi_5fversion_29',['ope_get_abi_version',['../group__encoding.html#gac2e46a09580a0f0c713582181e16ec40',1,'opusenc.h']]],
  ['ope_5fget_5fversion_5fstring_30',['ope_get_version_string',['../group__encoding.html#ga3bb110af9f05c22290b481bc9ad5c5ed',1,'opusenc.h']]],
  ['ope_5fpacket_5ffunc_31',['ope_packet_func',['../group__callbacks.html#gae9cbf8446492d21313b41f888b272632',1,'opusenc.h']]],
  ['ope_5fstrerror_32',['ope_strerror',['../group__encoding.html#ga0317f093c7b7d31fa5e3024f62faa3ce',1,'opusenc.h']]],
  ['ope_5fwrite_5ffunc_33',['ope_write_func',['../group__callbacks.html#ga2778e8766d550ac22ba1f134e783fe60',1,'opusenc.h']]],
  ['opusenccallbacks_34',['OpusEncCallbacks',['../structOpusEncCallbacks.html',1,'']]]
];
