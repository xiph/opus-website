var searchData=
[
  ['channel_5fcount_217',['channel_count',['../structOpusHead.html#ae3b3fc1a70e6b037c1d1d416fbdf2c8d',1,'OpusHead']]],
  ['close_218',['close',['../structOpusFileCallbacks.html#a04548cff8eda8ab0322f47cb702fe889',1,'OpusFileCallbacks']]],
  ['colors_219',['colors',['../structOpusPictureTag.html#a6fe2f98151fa32a8e2fd13f9309bd1a2',1,'OpusPictureTag']]],
  ['comment_5flengths_220',['comment_lengths',['../structOpusTags.html#aa99547abb03d7dbe0cd7095d8b706170',1,'OpusTags']]],
  ['comments_221',['comments',['../structOpusTags.html#a65c37166930a1f5d682fa6c863fc28c6',1,'OpusTags']]],
  ['content_5ftype_222',['content_type',['../structOpusServerInfo.html#a5f0120b006af1122cbdc72f3cbb68fd7',1,'OpusServerInfo']]],
  ['coupled_5fcount_223',['coupled_count',['../structOpusHead.html#a0b3e512ec24c989b0374358446197782',1,'OpusHead']]]
];
