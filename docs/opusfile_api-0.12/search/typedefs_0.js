var searchData=
[
  ['op_5fclose_5ffunc_251',['op_close_func',['../group__stream__callbacks.html#ga16c914ec90d301f125cdbeaa1ff57c2d',1,'opusfile.h']]],
  ['op_5fdecode_5fcb_5ffunc_252',['op_decode_cb_func',['../group__stream__decoding.html#ga81a50874a82484034c22dfeddce177e1',1,'opusfile.h']]],
  ['op_5fread_5ffunc_253',['op_read_func',['../group__stream__callbacks.html#ga9ffca429db1f3b77f2f303f1942188c3',1,'opusfile.h']]],
  ['op_5fseek_5ffunc_254',['op_seek_func',['../group__stream__callbacks.html#gae57cb396d1f193d3f4e7e56ddad7760e',1,'opusfile.h']]],
  ['op_5ftell_5ffunc_255',['op_tell_func',['../group__stream__callbacks.html#gaca012812dea4bc3a27b0c23575efecaf',1,'opusfile.h']]]
];
