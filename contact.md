---
layout: page
title: Contact
permalink: /contact/
---

# Contact

Development discussions and questions take place on the [Xiph.Org Opus mailing list][ml_xiph]
([opus@xiph.org][ml_xiph_mail]). Discussion related to the IETF process happen on the
[IETF mlcodec working group mailing list][ml_ietf] ([mlcodec@ietf.org][ml_ietf_mail]).

For archives of recent discussions, try:

-  [Xiph.Org Opus mailing list archives][ml_xiph_archive]
-  [IETF codec mailing list archives][ml_ietf_archive]
-  [Meeting minutes][meeting_min]

Informal development chat and support happens in [#opus on irc.libera.chat][irc_opus].
You can join the chat room through [this web page][irc_opus_web] if you don't
 have an IRC client.

If you find a bug, please report it in [gitlab][gitlab].

For anything non-confidential (e.g. general questions about Opus), please use
one of the mailing lists above.
For __confidential enquiries only__, you can contact one of the following developers directly:

-  [Jean-Marc Valin][j_m_valin] (Xiph.Org)
-  [Koen Vos][k_vos] (SMPL)
-  [Timothy B. Terriberry][t_terriberry] (Xiph.Org)


[ml_xiph]: http://lists.xiph.org/mailman/listinfo/opus
[ml_xiph_mail]: mailto:opus@xiph.org
[ml_ietf]: https://tools.ietf.org/wg/mlcodec/
[ml_ietf_mail]: mailto:mlcodec@ietf.org
[ml_xiph_archive]: http://lists.xiph.org/pipermail/opus/
[ml_ietf_archive]: https://www.ietf.org/mail-archive/web/codec/current/maillist.html
[meeting_min]: https://tools.ietf.org/wg/codec/minutes
[irc_opus]: ircs://irc.libera.chat:6697/opus
[irc_opus_web]: https://kiwiirc.com/nextclient/#ircs://irc.libera.chat/opus
[gitlab]: https://gitlab.xiph.org/xiph/opus/issues
[j_m_valin]: mailto:jmvalin@jmvalin.ca
[k_vos]: mailto:koenvos74@gmail.com
[t_terriberry]: mailto:tterribe@xiph.org
