<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="icon" href="https://www.xiph.org/images/logos/xiph.ico" type="image/x-icon"/>
    <link rel="stylesheet" title="default demosheet" href="demo.css" type="text/css"/>
    <link rel="stylesheet" title="default demosheet" media="print" href="demo_print.css" type="text/css"/>
    <title>Opus 1.5 Released</title>
    <script src="speech_demo.js"></script>
    <script src="dred_demo.js"></script>
  </head>

  <body onload="init_demo();init_dred_demo()">
    <div id="xiphlogo">
      <a href="https://www.xiph.org/"><img
      src="https://www.xiph.org/images/logos/fish_xiph_org.png"
      alt="Fish Logo and Xiph.org"/></a>
      <h1>Opus 1.5 Released</h1>
      <div class="or"><a href="https://people.xiph.org/~xiphmont/demo/index.html">(Other Xiph demos)</a></div>
    </div>


    <div>&nbsp;</div>
    <a href="https://opus-codec.org/">
    <img class="caption" style="width: 300px; padding-top: 1em; margin-left: auto; margin-right: auto;" src="opus-1.5_logo.png" alt="Banner"/></a>
    <div class="caption">
    Opus gets another major update with the release of version 1.5. This release brings quality improvements, including
    ML-based ones, while remaining fully compatible with RFC&nbsp;6716. Here are some of the most noteworthy upgrades.
    </div>

    <h2>Opus Gets a Serious Machine Learning Upgrade</h2>

    <p>This <a href="https://opus-codec.org/downloads/">1.5 release</a> is unlike any of the previous ones. It brings many new features
    that can improve quality and the general audio experience.
    That is achieved through machine learning. Although Opus has
    included machine learning &mdash; and even deep learning &mdash; before
    (e.g. for <a href="https://jmvalin.ca/opus/opus-1.3/">speech/music detection</a>),
    this is the first time it has used deep learning techniques to process or generate the signals
    themselves.</p>

    <p> Instead of designing a new ML-based codec
    from scratch, we prefer to improve Opus in a fully-compatible way.
    That is an important design goal for ML in Opus.
    Not only does that ensure Opus
    keeps working on older/slower devices, but it also provides an easy upgrade path. Deploying
    a new codec can be a long, painful process. Compatibility means that older and newer
    versions of Opus can coexist, while still providing the benefits of the new version
    when available.</p>

    <p>Deep learning also often gets associated with powerful GPUs, but
    in Opus, we have optimized everything such that it easily runs on most
    CPUs, including phones. We have been careful to avoid huge models (unlike LLMs with
    their hundreds of billions of parameters!). In the end, most users should not notice the extra cost,
    but people using older (5+ years) phones or microcontrollers might. For that reason, all new
    ML-based features are disabled by default in Opus 1.5. They require both a compile-time
    switch (for size reasons) and then a run-time switch (for CPU reasons).</p>

    <p>The following sections describe the new features enabled by ML.</p>

    <h2>Dealing with Packet Loss</h2>

    <p>Packet loss is one of the main annoyances one can encounter during a call. It does not
    matter how good the codec is if the packets do not get through.
    That's why most codecs have <i>packet loss concealment</i> (PLC) that can fill in for missing
    packets with plausible audio that just extrapolates what was being said and avoids leaving
    a hole in the audio (a common thing to hear with Bluetooth headsets). PLC is a place
    where ML can help a lot. Instead of using carefully hand-tuned concealment heuristics, we can just
    let a Deep Neural Network (DNN) do it. The technical details are in our
    <a href="https://arxiv.org/pdf/2205.05785.pdf">Interspeech 2022 paper</a>, for which we got the
    second place in the <a href="https://www.microsoft.com/en-us/research/academic-program/audio-deep-packet-loss-concealment-challenge-interspeech-2022/results/">Audio Deep Packet Loss Concealment Challenge</a>.</p>

    <p>When building Opus, using --enable-deep-plc will compile in the deep PLC code at a cost of
    about 1 MB in binary size.
    To actually enable it at run time, you will need to set the decoder complexity to 5 or more.
    Previously, only the encoder had a complexity knob, but the decoder is now getting one too.
    It can be set with the -dec_complexity option to opus_demo, or OPUS_SET_COMPLEXITY() in the
    API (like for the encoder).
    The extra complexity from running PLC at a high loss rate is about 1% of a laptop CPU core.
    Because deep PLC only affects the decoder, turning it on does not have any compatibility
    implications.
    </p>

    <h3>Deep REDundancy (DRED)</h3>

    <p>PLC is great for filling up occasional missing packets, but unfortunately
    packets often go missing in bursts. When that happens, entire phonemes or words are lost. Of course,
    new generative models could easily be used to seamlessly fill any gap with very plausible words, but
    we believe it is good to have the listener hear the <i>same</i> words that were spoken.
    The way to achieve that is through redundancy. Opus already includes
    a low-bitrate redundancy (LBRR) mechanism to transmit every speech frame twice, but only twice.
    While this helps reduce the impact
    of loss, there's only so much it can do for long bursts.
    </p>

    <p>That is where ML can help. We were certainly not the first to think about using
    ML to make a very low bitrate speech codec. However (we think) we are the first to
    design one that is optimized solely for transmitting redundancy. A regular codec needs to
    have short packets (typically 20 ms) to keep the latency low
    and it has to limit its use of prediction specifically to avoid making the packet
    loss problem even worse. For redundancy, we don't have these problems.
    Each packet will contain a large (up to 1 second) chunk of redundant audio
    that will be transmitted all at once.
    Taking advantage of that, the Opus Deep REDundancy (DRED) uses a rate-distortion-optimized
    variational autoencoder (RDO-VAE) to efficiently compress acoustic parameters in such a way that it can
    transmit one second of redundancy with about 12-32 kb/s overhead.
    Every 20-ms packet is effectively transmitted <i>50 times</i> at a cost similar
    to the existing LBRR.
    See this <a href="https://www.amazon.science/blog/neural-encoding-enables-more-efficient-recovery-of-lost-audio-packets">
    demo</a> for a high-level overview of the science behind DRED, or read the
    <a href="https://arxiv.org/pdf/2212.04453.pdf">ICASSP 2023 paper</a> for all the details and math
    behind it.
    </p>

    <a href="dred_results.png"><img class="caption" style="width: 400px;" src="dred_results.png" alt="recurrent units"></a>
    <div class="caption">
    Subjective testing (MOS) results measuring the improvement provided by DRED with one second
    redundancy for a range of
    realistic packet loss conditions.
    The results show that DRED achieves much higher quality than what either neural PLC alone,
    or LBRR with neural PLC can achieve.
    When DRED is combined with LBRR, the quality approaches that of the no-loss case.
    In these tests, we used 24&nbsp;kb/s for the <em>base</em> Opus layer, 16&nbsp;kb/s extra for LBRR,
    and 32&nbsp;kb/s extra for DRED.
    </div>

    <p>Use the --enable-dred configure option (which automatically turns on --enable-deep-plc) to
    enable DRED.
    Doing so increases the binary size by about 2&nbsp;MB, with a run-time cost around 1% like for deep PLC.
    Beware that DRED is not yet standardized and the version included in Opus&nbsp;1.5 will
    not be compatible with the final version.
    That being said, it is still safe to experiment with it in applications since the bitstream
    carries an experiment version number and any version incompatibility will be detected and simply cause
    the DRED payload to be ignored (no erroneous decoding or loud noises).</p>


    <h3>Neural Vocoder</h3>

    <p>The very low complexity of deep PLC and DRED is made possible by new neural vocoder technology
    we created specifically for this project. The original papers linked above used a
    <a href="https://arxiv.org/pdf/2202.11169.pdf">highly-optimized</a> version of the original
    <a href="https://jmvalin.ca/demo/lpcnet/">LPCNet vocoder</a>, but even that was not quite
    fast enough. So we came up with a new framewise autoregressive generative
    adversarial network (FARGAN) vocoder that uses pitch prediction to achieve
    a complexity of 600 MFLOPS: 1/5 of LPCNet. That
    allows it to run with less than 1% of a CPU core on laptops or even recent phones.
    <b>Updated</b>: Our
    <a href="https://arxiv.org/pdf/2405.21069">paper on FARGAN</a> was accepted in IEEE Signal Processing Letters.</p>

    <h2>Low-Bitrate Speech Quality Enhancement</h2>

    <p>Given enough bits, most speech codecs &mdash; including Opus &mdash; are able to reach a quality
      level close to transparency.
      Unfortunately, the real world sometimes doesn't give us "enough bits". Suddenly, the coding
      artifacts can become audible, or even annoying.
      The classical approach to mitigate this problem is to apply simple, handcrafted
      postfilters that reshape the coding noise to make it less noticeable.
      While those postfilters usually provide a noticeable improvement, their effectiveness is limited. They
      can't work wonders.
    </p>

    <p>
      The rise of ML and DNNs has produced a number of new and much more powerful enhancement methods,
      but these are typically large, high in complexity, and cause additional decoder delay.
      Instead, we went for a different approach: start with the tried-and-true postfilter idea
      and sprinkle just enough DNN magic on top of it.
      Opus 1.5 includes two enhancement methods: the Linear Adaptive Coding Enhancer (LACE) and a
      Non-Linear variation (NoLACE).
      From the signal point of view, LACE is very similar to a classical postfilter.
      The difference comes from a DNN that
      optimizes the postfilter coefficients on-the-fly based on all the data available to the decoder.
      The audio itself never goes through the DNN.
      The result is a small and very-low-complexity model (by DNN standards) that can run even
      on older phones. An explanation of the internals of LACE is given in this short
      <a href="https://www.youtube.com/watch?v=W47qh9Wp9E0">video presentation</a> and more technical
      details can be found in the corresponding <a href="https://arxiv.org/abs/2307.06610">WASPAA 2023 paper</a>.
      NoLACE is an extension of LACE that requires more computation but is
      also much more powerful due to extra non-linear signal processing.
      It still runs without significant overhead on
      recent laptop and smartphone CPUs. Technical details about NoLACE are given in the corresponding
      <a href="https://arxiv.org/abs/2309.14521">ICASSP 2024 paper</a>.
    </p>

    <a href="nolace_results.png"><img class="caption" style="width: 452px;" src="nolace_results.png" alt="recurrent units"></a>
    <div class="caption">
    Subjective testing (MOS) results comparing the speech decoded from the default
    decoder to the enhanced speech produced by LACE and NoLACE from that same decoder.
    The uncompressed speech has a MOS of 4.06.
    The results show that using NoLACE, Opus is now perfectly usable down to 6 kb/s.
    At 9 kb/s, NoLACE-enhanced speech is already close to transparency, and better than
    the non-enhanced 12 kb/s.
    </div>

    <p>
      To try LACE and NoLACE, just add the --enable-osce configure flag when building Opus.
      Then, to enable LACE at run-time, set the decoder complexity to 6.
      Set it to 7 or higher to enable NoLACE instead of LACE. Building with --enable-osce increases
      the binary size by about 1.6 MB, roughly 0.5 MB for LACE and 1.1 MB for NoLACE. The LACE model has a
      complexity of 100 MFLOPS which leads to a run-time cost of ~0.15% CPU usage. The NoLACE model has a complexity
      of 400 MFLOPS which corresponds to a run-time cost of ~0.75% CPU usage.
      LACE and NoLACE are currently only applied when the frame size is 20 ms (the default) and the bandwidth
      is at least wideband.
      Although LACE and NoLACE have not yet been standardized, turning them on does not have
      compatibility implications since the enhancements are independent of the encoder.
    </p>

    <h3>Samples</h3>

    <p>OK, nice graphs, but how does it actually sound? The following samples demonstrate
    the effect of LACE or NoLACE on Opus wideband speech quality at different bitrates. We recommend listening with good headphones,
    especially for higher bitrates. </p>

    <div class="comparison">
      <audio controls="" id="speech_player" src="samples/female_nolace_12k.wav">
        Your browser does not support the audio tag.
      </audio>
      <div>
      <p class="compare_label" style="display: inline-block">Select sample</p>
      <ul class="sample">
        <li onclick="setSpeechSample(0, this);" id="speech_default_sample" class="selected">Female</li>
        <li onclick="setSpeechSample(1, this);">Male</li>
      </ul>
      </div>
      <div>
      <p class="compare_label" style="display: inline-block">Select enhancement</p>
      <ul class="codec">
        <li onclick="setSpeechCodec(1, this);">None</li>
        <li onclick="setSpeechCodec(2, this);">LACE</li>
        <li onclick="setSpeechCodec(3, this);" id="speech_default_codec" class="selected">NoLACE</li>
        <li onclick="setSpeechCodec(4, this);">Uncompressed</li>
      </ul>
      </div>
      <div>
      <p class="compare_label" style="display: inline-block">Select bitrate</p>
      <ul class="bitrate" id="speech_bitrate_selector">
        <li onclick="setSpeechRate(0, this);">6 kb/s</li>
        <li onclick="setSpeechRate(1, this);" id="speech_default_rate" class="selected">9 kb/s</li>
        <li onclick="setSpeechRate(2, this);">12 kb/s</li>
      </ul>
      </div>
      <p>Select where to start playing when selecting a new sample</p>
      <button type="button" onclick="speech_norestart();">Keep playing</button>
      <button type="button" onclick="speech_setrestart();">Set current position as restart point</button>
      <p style="display: inline-block" id="speech_restart_string">Player will <b>continue</b> when changing sample.</p>
    </div>
    <div class="caption">
      <p>Demonstrating the effect of LACE and NoLACE on speech quality at 6, 9, and 12 kb/s.
    </div>

    <h2>WebRTC Integration</h2>

    <p>Using the deep PLC or the quality enhancements should typically require only minor
    code changes. DRED is an entirely different story. It requires closer integration with
    the jitter buffer to ensure that redundancy gets used.</p>
    <p>In a real-time communications system, the size of the jitter buffer determines the
    maximum amount of packet arrival lateness that can be tolerated without producing
    an audible gap in audio playout.
    In the case of packet loss, we can treat the DRED data similarly to
    late arriving audio packets. We take care to only insert this data into the jitter
    buffer if we have observed prior loss. In ideal circumstances, an adaptive jitter
    buffer (like NetEq used in WebRTC) will try to minimize its size in order to preserve
    interactive latency. If data arrives too late for playback, there will be an audible
    gap, but the buffer will then grow to accommodate the new nominal lateness. If network
    conditions improve the buffer can shrink back down, using time scaling to play the
    audio at a slightly faster rate. In the case of DRED, there will always be a loss vs.
    latency tradeoff. In order to make use of the DRED data and cover prior lost packets,
    we will need to tolerate a larger jitter buffer.
    But because we treat DRED similarly to late packet arrival, we can take advantage
    of the existing adaptation in NetEq to provide a reasonable compromise in loss vs. latency.
    </p>

    <p>You can try out DRED using the patches in our <a href="https://github.com/xiph/webrtc-opus-ng/tree/opus-ng">
    webrtc-opus-ng</a> fork of the Google WebRTC repository.
    Using these patches, we were able to evaluate how DRED compares to other approaches.
    And yes, it still works well even with 90% loss.
    See the results below.
    </p>

    <a href="dred_plcmos.png"><img class="caption" style="width: 600px;" src="dred_plcmos.png" alt="DRED PLCMOSv2 results"></a>
    <div class="caption">
    Objective evaluation of different redundancy schemes under simulated realistic
    packet loss (see <a href="#lossgen">Realistic Loss Simulator</a> below) using Microsoft's
    <a href="https://github.com/microsoft/PLC-Challenge/tree/main/PLCMOS">PLCMOS v2</a> (higher is better).
    All conditions use 48 kb/s, except for DRED+LBRR which uses 64 kb/s to fully take advantage
    of both forms of redundancy.
    Results show that even under extremely lossy conditions, DRED is able to maintain
    acceptable quality.
    It may look strange that the DRED quality increases past 60% loss, but that can be explained by
    the reduced amount of switching between regular packets and DRED redundancy.
    </div>

    <h3>Samples</h3>

    <p>Of course, hearing is believing, so here are some samples produced with the WebRTC patches.
    These should be close to what one might experience during a meeting when packets start to drop.
    Notice some gaps at the beginning as the jitter buffer adapts and is then able to take full advantage
    of DRED. </p>

    <div class="comparison">
      <audio controls="" id="dred_player" src="samples/dred/spe48_loss_50_dred_48kbps.wav">
        Your browser does not support the audio tag.
      </audio>
      <div>
      <p class="compare_label" style="display: inline-block">Select loss rate</p>
      <ul class="codec">
        <li onclick="setDREDPercent(0, this);">0%</li>
        <li onclick="setDREDPercent(1, this);">10%</li>
        <li onclick="setDREDPercent(2, this);">20%</li>
        <li onclick="setDREDPercent(3, this);">30%</li>
        <li onclick="setDREDPercent(4, this);">40%</li>
        <li onclick="setDREDPercent(5, this);" id="dred_default_percent" class="selected">50%</li>
        <li onclick="setDREDPercent(6, this);">60%</li>
        <li onclick="setDREDPercent(7, this);">70%</li>
        <li onclick="setDREDPercent(8, this);">80%</li>
        <li onclick="setDREDPercent(9, this);">90%</li>
      </ul>
      </div>
      <div>
      <p class="compare_label" style="display: inline-block">Select redundancy</p>
      <ul class="bitrate" id="dred_bitrate_selector">
        <li onclick="setDREDSample(0, this);">None (48 kb/s)</li>
        <li onclick="setDREDSample(1, this);">LBRR (48 kb/s)</li>
        <li onclick="setDREDSample(2, this);" id="dred_default_sample" class="selected">DRED (48 kb/s)</li>
        <li onclick="setDREDSample(3, this);">DRED+LBRR (64 kb/s)</li>
      </ul>
      </div>
      <p>Select where to start playing when selecting a new sample</p>
      <button type="button" onclick="dred_norestart();">Keep playing</button>
      <button type="button" onclick="dred_setrestart();">Set current position as restart point</button>
      <p style="display: inline-block" id="dred_restart_string">Player will <b>continue</b> when changing sample.</p>
    </div>
    <div class="caption">
      <p>Evaluating the effectiveness of the different redundancy options. These audio samples are generated
      using real packet loss traces with the entire WebRTC stack.
    </div>

    <h2>IETF and Standardization</h2>

    <p>To ensure compatibility with the existing standard and future extensions of Opus,
    this work is being conducted within the newly-created IETF
    <a href="https://datatracker.ietf.org/wg/mlcodec/">mlcodec</a> working group.
    This effort is currently focused on three topics:
    a generic extension mechanism for Opus, deep redundancy, and speech coding enhancement.
    </p>

    <h3>Extension Format</h3>

    <p>The new DRED mechanism requires adding extra information to Opus packets while
    allowing an older decoder that does not know about DRED to still decode the regular Opus data.
    We found that the best way to achieve that was through the Opus padding mechanism.
    In the original specification, padding was added to make it possible to make a packet
    bigger if needed (e.g., to meet a constant bitrate even when the encoder produced fewer
    bits than the target).
    Thanks to padding, we can transmit extra information in a packet in a way that an
    older decoder will just not see (so it won't get confused).
    Of course, if we're going to all that trouble, we might as well make sure we're also
    able to handle any future extensions.
    Our <a href="https://datatracker.ietf.org/doc/draft-ietf-mlcodec-opus-extension/">
    Opus extension Internet-Draft</a> defines a format <i>within</i> the Opus padding
    that can be used to transmit both deep redundancy, but also any future extension
    that may become useful. See our
    <a href="https://datatracker.ietf.org/meeting/118/materials/slides-118-mlcodec-opus-extension-mechanism-00.pdf">presentation at IETF&nbsp;118</a>
    for diagrams of how the extensions fit within an Opus packet.
    </p>

    <h3>DRED Bitstream</h3>

    <p>We are also working on standardizing DRED. Standardizing an ML algorithm is
    challenging because of the tradeoff between compatibility and extensibility.
    That's why our <a href="https://datatracker.ietf.org/doc/draft-valin-opus-dred/">
    DRED Internet-Draft</a> describes how to decode extension bits into acoustic features,
    but leaves implementers free to make both better encoders and also better
    vocoders that may further improve on the quality and/or complexity.</p>

    <h3>Enhancement</h3>
    <p>
      For enhancement, we also follow the general strategy to standardize as little as
      possible, since we also expect future research to produce better methods than we
      currently have. That's why we will specify requirements an enhancement method like
      LACE or NoLACE should satisfy in order to be allowed in an opus decoder rather than
      specifying the methods themselves.
      A corresponding <a href="https://datatracker.ietf.org/doc/draft-buethe-opus-speech-coding-enhancement/">
      enhancement Internet-Draft</a>
      has already been created for that purpose.
    </p>

    <h2>Other Improvements</h2>

    <p>Here are briefly some other changes in this release.</p>

    <h3>AVX2 Support</h3>

    <p>Opus now has support and run-time detection for AVX2.
    On machines that support AVX2/FMA (from around 2015 or newer), both the new DNN
    code and the SILK encoder will be significantly faster thanks to the use of
    256-bit SIMD.</p>

    <h3>More NEON Optimizations</h3>

    <p>Existing ARMv7 Neon optimization were re-enabled for AArch64, resulting
    in more efficient encoding.
    The new DNN code can now take advantage of the Arm <i>dot product</i> extensions
    that significantly speed up 8-bit integer dot products on a Cortex-A75 or newer
    (~5 year old phones). Support is detected at run-time, so
    these optimizations are safe on all Arm CPUs.</p>

    <a name="lossgen"></a><h3>Realistic Loss Simulator</h3>
    <p>As a side effect of trying to tune the DRED encoder to maximize quality, we realized
    we needed a better way of simulating packet loss.
    For some purposes, testing with random loss patterns (like tossing a coin repeatedly)
    can be good enough, but since DRED is specifically designed to handle burst loss (which
    is rare with independent random losses) we needed something better.
    As part of the Audio Deep Packet Loss Concealment Challenge, Microsoft
    <a href="https://github.com/microsoft/PLC-Challenge">made available</a>
    some more realistic recorded packet loss traces.
    A drawback of such real data is that one cannot control the percentage of loss
    or generate sequences longer than those in the dataset.
    So we trained a generative packet loss model that can simulate realistic losses with
    a certain target overall percentage of loss.
    Packet loss traces are quite simple and our generative
    model fits in fewer than 10,000 parameters.
    To simulate loss with opus_demo, you need to build with --enable-lossgen.
    Then add -sim-loss &lt;percentage&gt; to the opus_demo command line.
    Note that the loss generator is just an initial design, so feedback is welcome.
    </p>

    <p>
    Because we believe that this loss generator can be useful to other applications,
    we have made it easy to extract it from Opus and use it in other applications.
    The main source file for the generator is <a href="https://gitlab.xiph.org/xiph/opus/-/blob/main/dnn/lossgen.c">
    dnn/lossgen.c</a>. Comments in the file contain information about the other
    dependencies needed for the loss generator.</p>

    <h2> Conclusion </h2>
    <p>
      We hope we demonstrated how our new ML-based tools substantially
      improve error robustness and speech quality with a very modest
      performance impact and without sacrificing compatibility.
      And we're only getting started. There's still more to come.
      We encourage everyone to try out these new features for themselves.
      Please <a href="https://www.opus-codec.org/contact/">let us know</a> about your experience (good or bad)
      so we can continue to improve them.
      Enjoy!
    </p>

    <address style="clear: both;">&mdash;The Opus development team
      <br>March 4th, 2024
    </address>


    <h2>Additional Resources</h2>
    <ol style="padding-bottom: 1em;">
      <li>First and foremost: <a href="https://www.opus-codec.org/">The Opus Project Homepage</a></li>
      <li>The basic Opus techniques for music coding are described in the AES paper:
      <a href="https://jmvalin.ca/papers/aes135_opus_celt.pdf">High-Quality, Low-Delay Music Coding in
      the Opus Codec</a></li>
      <li>The basic Opus techniques for speech coding are described in this other AES paper:
      <a href="https://jmvalin.ca/papers/aes135_opus_silk.pdf">Voice Coding with Opus</a></li>

      <li>Join our development discussion in <a href="irc://irc.libera.chat/opus">#opus at irc.libera.chat</a> (&rarr;<a href="https://web.libera.chat/#opus" onclick="document.getElementById('chatbox').innerHTML='<iframe src=\'https://web.libera.chat/#opus\' width=800 height=600/>';return false;">web interface</a>)</li>
    </ol>
      <div id="chatbox" style="text-align:center;"></div>
    <hr />

    <div class="et" style="position: relative;">
      <div class="etleft" style="width: 132px; height: 75px;">
      </div>
      <div class="etcenter">
        <div class="etcontent">
          (C) Copyright 2024 Xiph.Org Foundation
        </div>
      </div>

    </div>

  </body>
</html>
