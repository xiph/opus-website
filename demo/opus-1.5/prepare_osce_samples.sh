#!/bin/bash


# check if some commands are available
if ! command -v sox &> /dev/null
then
    echo "aborting: script requires sox"
    exit 1
fi

if ! command -v git &> /dev/null
then
    echo "aborting: script requires git"
    exit 1
fi


# checkout latest opus main as placeholder for 1.5
git clone https://gitlab.xiph.org/xiph/opus.git
(cd opus && ./autogen.sh && ./configure --disable-shared --enable-osce && make -j)
OPUS1P5=./opus/opus_demo


# download EBU SQAM CD
mkdir -p sqam
(cd sqam && wget https://tech.ebu.ch/files/live/sites/tech/files/shared/testmaterial/SQAM_FLAC.zip && unzip SQAM_FLAC.zip)


# prepare input
mkdir -p samples
sox sqam/49.flac -c 1 -b 16 -e signed-integer -r 16000 samples/female_ref.raw trim 0 20
sox sqam/49.flac -c 1 -b 16 -e signed-integer -r 16000 samples/female_ref.wav trim 0 20
sox sqam/49.flac -c 1 -b 16 -e signed-integer -r 16000 samples/female_ref.flac trim 0 20

sox sqam/50.flac -c 1 -b 16 -e signed-integer -r 16000 samples/male_ref.raw trim 0 20
sox sqam/50.flac -c 1 -b 16 -e signed-integer -r 16000 samples/male_ref.wav trim 0 20
sox sqam/50.flac -c 1 -b 16 -e signed-integer -r 16000 samples/male_ref.flac trim 0 20


BITRATES=( 6000 9000 12000 )
for br in ${BITRATES[@]}
do
    for prefix in male female
    do
        let br2=br/1000
        $OPUS1P5 voip 16000 1 $br -bandwidth WB -dec_complexity 5 samples/${prefix}_ref.raw samples/${prefix}_opus15_${br2}k.raw
        $OPUS1P5 voip 16000 1 $br -bandwidth WB -dec_complexity 6 samples/${prefix}_ref.raw samples/${prefix}_lace_${br2}k.raw
        $OPUS1P5 voip 16000 1 $br -bandwidth WB -dec_complexity 7 samples/${prefix}_ref.raw samples/${prefix}_nolace_${br2}k.raw

        sox -b 16 -r 16000 -e signed-integer samples/${prefix}_opus15_${br2}k.raw samples/${prefix}_opus15_${br2}k.wav
        sox -b 16 -r 16000 -e signed-integer samples/${prefix}_lace_${br2}k.raw samples/${prefix}_lace_${br2}k.wav
        sox -b 16 -r 16000 -e signed-integer samples/${prefix}_nolace_${br2}k.raw samples/${prefix}_nolace_${br2}k.wav

        sox -b 16 -r 16000 -e signed-integer samples/${prefix}_opus15_${br2}k.raw samples/${prefix}_opus15_${br2}k.flac
        sox -b 16 -r 16000 -e signed-integer samples/${prefix}_lace_${br2}k.raw samples/${prefix}_lace_${br2}k.flac
        sox -b 16 -r 16000 -e signed-integer samples/${prefix}_nolace_${br2}k.raw samples/${prefix}_nolace_${br2}k.flac
    done
done

rm samples/male_*.raw samples/female_*.raw
rm -rf opus
rm -rf sqam