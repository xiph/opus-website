
var selected_speech_sample;
var selected_speech_rate;
var selected_speech_codec;
var speech_sample_id=-1;
var speech_codec_id=-1;
var speech_rate_id=-1;
var speech_pos = -1;

var speech_sample = ["female", "male"];
var speech_ext = ["opus", "opus", "opus", "opus", "flac"];
var speech_codecs = ["opus1", "opus15", "lace", "nolace", "ref"]
var speech_rates = ["6k", "9k", "12k"];

function updateSpeechClip() {
  if (speech_rate_id == -1 || speech_sample_id == -1) return;
  var audio = document.getElementById("speech_player");
  if (speech_codec_id != 4) rate_suffix = "_" + speech_rates[speech_rate_id];
  else rate_suffix = "";
  name = "https://media.xiph.org/opus/samples/opus-1.5/osce/" + speech_sample[speech_sample_id] + "_" + speech_codecs[speech_codec_id] + rate_suffix;
  var extension = "wav";
  /*console.log("extension = " + extension);*/
  name = name + "." + extension;
  var playing = !(audio.paused);
  var position = audio.currentTime;
  if (speech_pos >= 0)
    position = speech_pos;

  console.log("playing " + name);
  audio.src = name;

  audio.addEventListener("loadedmetadata",
                           function() {
                               // don't set if zero
                               if(position>0)
                                   audio.currentTime = position;
                               if(playing)
                                   audio.play();
                               this.removeEventListener("loadedmetadata",arguments.callee,true);
                           }, true);
  
  audio.load();
  /*console.log(audio.canPlayType('audio/ogg; codecs="opus"'));
  console.log(audio.canPlayType("audio/mpeg;"));
  console.log(audio.canPlayType('audio/wav; codecs="1"'));*/
}

function setSpeechSample(sample_id, item) {
  if (speech_sample_id == sample_id) return;
  speech_sample_id = sample_id;
  if (selected_speech_sample) selected_speech_sample.classList.remove("selected");

  item.classList.add("selected");
  selected_speech_sample = item;
  updateSpeechClip();
}

function setSpeechCodec(codec_id, item) {
  if (speech_codec_id == codec_id) return;
  speech_codec_id = codec_id;
  if (selected_speech_codec) selected_speech_codec.classList.remove("selected");
  item.classList.add("selected");
  selected_speech_codec = item;
  updateSpeechClip();
}

function setSpeechRate(rate_id, item) {
  if (speech_rate_id == rate_id) return;
  speech_rate_id = rate_id;
  if (selected_speech_rate) selected_speech_rate.classList.remove("selected");
  item.classList.add("selected");
  selected_speech_rate = item;
  updateSpeechClip();
}

function speech_norestart() {
  speech_pos = -1;
  document.getElementById("speech_restart_string").innerHTML = "Player will <b>continue</b> when changing sample.";
}

function speech_setrestart() {
  speech_pos = document.getElementById("speech_player").currentTime;
  document.getElementById("speech_restart_string").innerHTML = "Player will <b>restart at " + speech_pos.toFixed(2) + " seconds</b> when changing sample.";
}

function init_demo() {
  setSpeechCodec(3, document.getElementById("speech_default_codec"));
  setSpeechSample(0, document.getElementById("speech_default_sample"));
  setSpeechRate(1, document.getElementById("speech_default_rate"));
  speech_setrestart();

}
