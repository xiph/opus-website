#!/usr/bin/env python
import os

import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    values = [
# p0
3.886802040020625,
3.862083460927009,
3.824843565225601,
3.8305853110949197,
# p10
3.452165739417077,
3.6549411923090616,
3.545636534253756,
3.6636667352120083,
#p20
3.103566276629766,
3.4586182589928307,
3.367962013085683,
3.5541347241401673,
#p30
2.7545064987341563,
3.2323217480977378,
3.250446905692418,
3.445581300298373,
#p40
2.3895258876085284,
3.019523400425911,
3.076975355307261,
3.342642618020376,
#p50
2.082639576673508,
2.8268090811173123,
2.976429570277532,
3.2614177406231564,
#p60
1.8169795484145481,
2.632349169254303,
2.883158590396245,
3.197260335961978,
#p70
1.5538731286923093,
2.2341453177134194,
2.9094991840918865,
3.0972533466418586,
#p80
1.3610978531042734,
1.689580816189448,
2.976747566779455,
2.9596427860657375,
#p90
1.2808318444093068,
1.379787104765574,
3.158671729405721,
2.998843259811401,
]

    lim=90
    plt.rcParams.update({"font.family": "serif"})
    plt.rcParams.update({"font.serif": "cmr10"})
    plt.rcParams.update({'font.size': 16})
    fig, ax = plt.subplots(figsize=(9, 5), dpi=96)
    plt.xlabel("% Loss", weight='bold')
    plt.ylabel("PLCMOS Score", weight='bold')
    ax.plot(np.arange(0,lim+1,10), values[::4], 'X', markersize=8, linestyle='dotted', linewidth=2, label='None')
    ax.plot(np.arange(0,lim+1,10), values[1::4], 'o', markersize=8, linestyle='dashdot', linewidth=2, label='LBRR')
    ax.plot(np.arange(0,lim+1,10), values[2::4], 'd', markersize=8, linestyle='dashed', linewidth=2, label='DRED')
    ax.plot(np.arange(0,lim+1,10), values[3::4], 's', markersize=8, linestyle='solid', linewidth=2, label='LBRR+DRED')
    
    ax.set_xticks(np.arange(0,90+1,10))
    ax.set_xlim([0, 93])
    ax.set_ylim([1.22, 4.15])
    ax.legend()
    ax.grid()
#    plt.suptitle('PLCMOSv2 Evaluation - Simulated Loss - Neural PLC', weight='bold', size=18)
    
    plt.savefig('lossgen_plcmos_v2.pdf')
    #plt.show()

    print("Done!")
