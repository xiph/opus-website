
var selected_dred_sample;
var selected_dred_percent;
var dred_sample_id=-1;
var dred_percent_id=-1;
var dred_pos = -1;

var dred_sample = ["nofec_48kbps", "fec_48kbps", "dred_48kbps", "fec_dred_64kbps"];

function updateDREDClip() {
  if (dred_percent_id == -1 || dred_sample_id == -1) return;
  var audio = document.getElementById("dred_player");
  name = "https://media.xiph.org/opus/samples/opus-1.5/dred/spe48_loss_" + 10*dred_percent_id + "_" + dred_sample[dred_sample_id];
  var extension = "wav";
  if (speech_codec_id != 4 && audio.canPlayType('audio/ogg; codecs="opus"')) extension = "opus";
  else if (audio.canPlayType('audio/flac;')) extension = "flac";
  else extension = "wav";
  /*console.log("extension = " + extension);*/
  name = name + "." + extension;
  var playing = !(audio.paused);
  var position = audio.currentTime;
  if (dred_pos >= 0)
    position = dred_pos;

  console.log("playing " + name);
  audio.src = name;

  audio.addEventListener("loadedmetadata",
                           function() {
                               // don't set if zero
                               if(position>0)
                                   audio.currentTime = position;
                               if(playing)
                                   audio.play();
                               this.removeEventListener("loadedmetadata",arguments.callee,true);
                           }, true);
  
  audio.load();
  /*console.log(audio.canPlayType('audio/ogg; codecs="opus"'));
  console.log(audio.canPlayType("audio/mpeg;"));
  console.log(audio.canPlayType('audio/wav; codecs="1"'));*/
}

function setDREDSample(sample_id, item) {
  if (dred_sample_id == sample_id) return;
  dred_sample_id = sample_id;
  if (selected_dred_sample) selected_dred_sample.classList.remove("selected");

  item.classList.add("selected");
  selected_dred_sample = item;
  updateDREDClip();
}

function setDREDPercent(percent_id, item) {
  if (dred_percent_id == percent_id) return;
  dred_percent_id = percent_id;
  if (selected_dred_percent) selected_dred_percent.classList.remove("selected");
  item.classList.add("selected");
  selected_dred_percent = item;
  updateDREDClip();
}

function dred_norestart() {
  dred_pos = -1;
  document.getElementById("dred_restart_string").innerHTML = "Player will <b>continue</b> when changing sample.";
}

function dred_setrestart() {
  dred_pos = document.getElementById("dred_player").currentTime;
  document.getElementById("dred_restart_string").innerHTML = "Player will <b>restart at " + dred_pos.toFixed(2) + " seconds</b> when changing sample.";
}

function init_dred_demo() {
  setDREDSample(2, document.getElementById("dred_default_sample"));
  setDREDPercent(5, document.getElementById("dred_default_percent"));
  dred_setrestart();
}
